package com.jago.loan

import org.jeasy.random.EasyRandom

val random = EasyRandom()

inline fun <reified T> EasyRandom.next(): T = random.nextObject(T::class.java)
