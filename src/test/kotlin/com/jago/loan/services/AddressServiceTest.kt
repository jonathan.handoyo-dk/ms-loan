package com.jago.loan.services

import com.jago.loan.domains.Region
import com.jago.loan.domains.RegionRepository
import com.jago.loan.next
import com.jago.loan.random
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import reactor.core.publisher.Flux
import reactor.kotlin.test.test

@Suppress("ReactiveStreamsUnusedPublisher")
@ExtendWith(MockKExtension::class)
internal class AddressServiceTest {

  private val regionRepository = mockk<RegionRepository>()
  private val addressService = AddressService(regionRepository)

  @Nested
  inner class GetByType {

    @Test
    internal fun `Given Type - When exists - Should return Flux`() {
      // Given
      val type = random.next<Region.Type>()
      val region1 = random.next<Region>()
      val region2 = random.next<Region>()
      every { regionRepository.findAllByType(type) } returns Flux.just(region1, region2)

      // When-Then
      addressService.getAllByType(type).test()
        .expectSubscription()
        .expectNext(
          region1,
          region2
        )
        .expectComplete()
        .verify()
    }

    @Test
    internal fun `Given Type - When not exists - Should return empty Flux`() {
      // Given
      val type = random.next<Region.Type>()
      every { regionRepository.findAllByType(type) } returns Flux.empty()

      // When-Then
      addressService.getAllByType(type).test()
        .expectSubscription()
        .expectComplete()
        .verify()
    }

    @Test
    internal fun `Given Type - When error - Should return error Flux`() {
      // Given
      val type = random.next<Region.Type>()
      every { regionRepository.findAllByType(type) } returns Flux.error { RuntimeException("boom!") }

      // When-Then
      addressService.getAllByType(type).test()
        .expectSubscription()
        .expectError()
        .verify()
    }
  }

  @Nested
  inner class GetByPrefixAndType {

    @Test
    internal fun `Given Prefix and Type - When exists - Should return Flux`() {
      val prefix = random.next<String>()
      val type = random.next<Region.Type>()
      val region1 = random.next<Region>()
      val region2 = random.next<Region>()
      every { regionRepository.findAllByTypeAndCodeStartingWith(type, prefix) } returns Flux.just(region1, region2)

      // When-Then
      addressService.getAllByPrefixAndType(prefix, type).test()
        .expectSubscription()
        .expectNext(
          region1,
          region2
        )
        .expectComplete()
        .verify()
    }

    @Test
    internal fun `Given Prefix and Type - When not exists - Should return empty Flux`() {
      // Given
      val prefix = random.next<String>()
      val type = random.next<Region.Type>()
      every { regionRepository.findAllByTypeAndCodeStartingWith(type, prefix) } returns Flux.empty()

      // When-Then
      addressService.getAllByPrefixAndType(prefix, type).test()
        .expectSubscription()
        .expectComplete()
        .verify()
    }

    @Test
    internal fun `Given Prefix and Type - When error - Should return error Flux`() {
      // Given
      val prefix = random.next<String>()
      val type = random.next<Region.Type>()
      every {
        regionRepository.findAllByTypeAndCodeStartingWith(
          type,
          prefix
        )
      } returns Flux.error { RuntimeException("boom!") }

      // When-Then
      addressService.getAllByPrefixAndType(prefix, type).test()
        .expectSubscription()
        .expectError()
        .verify()
    }
  }
}
