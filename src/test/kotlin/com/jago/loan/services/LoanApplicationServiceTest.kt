package com.jago.loan.services

import com.jago.loan.domains.LoanApplicationRepository
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@Suppress("ReactiveStreamsUnusedPublisher")
@ExtendWith(MockKExtension::class)
internal class LoanApplicationServiceTest {

  private val loanApplicationRepository = mockk<LoanApplicationRepository>()
  private val loanApplicationService = LoanApplicationService(
    loanApplicationRepository
  )

  @Nested
  inner class GetAllByCustomer {

    @Test
    internal fun `Given CustomerRef - When exists - Should return Flux`() {
    }

    @Test
    internal fun `Given CustomerRef - When not exists - Should return empty Flux`() {
    }

    @Test
    internal fun `Given CustomerRef - When error - Should return error Flux`() {
    }
  }

  @Nested
  inner class GetAllByCustomerAndStatus {

    @Test
    internal fun `Given CustomerRef - And LoanApplication#Status - When exists - Should return Flux`() {
    }

    @Test
    internal fun `Given CustomerRef - And LoanApplication#Status - When not exists - Should return empty Flux`() {
    }

    @Test
    internal fun `Given CustomerRef - And LoanApplication#Status - When error - Should return error Flux`() {
    }
  }
}
