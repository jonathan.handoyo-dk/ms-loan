package com.jago.loan.services

import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@Suppress("ReactiveStreamsUnusedPublisher")
@ExtendWith(MockKExtension::class)
internal class LoanOfferServiceTest {

  @Nested
  inner class GetAllByCustomer {

    @Test
    internal fun `Given Customer - When exists - Should return Flux`() {
    }

    @Test
    internal fun `Given Customer - When not exists - Should return empty Flux`() {
    }

    @Test
    internal fun `Given Customer - When error - Should return error Flux`() {
    }
  }

  @Nested
  inner class GetAllByLoanProduct {

    @Test
    internal fun `Given LoanProduct - When exists - Should return Flux`() {
    }

    @Test
    internal fun `Given LoanProduct - When not exists - Should return empty Flux`() {
    }

    @Test
    internal fun `Given LoanProduct - When error - Should return error Flux`() {
    }
  }
}
