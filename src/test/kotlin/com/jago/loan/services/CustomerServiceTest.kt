package com.jago.loan.services

import com.jago.loan.domains.Customer
import com.jago.loan.domains.CustomerRepository
import com.jago.loan.next
import com.jago.loan.random
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import reactor.kotlin.test.test

@Suppress("ReactiveStreamsUnusedPublisher")
@ExtendWith(MockKExtension::class)
internal class CustomerServiceTest {

  private val customerRepository = mockk<CustomerRepository>()
  private val customerService = CustomerService(customerRepository)

  @Nested
  inner class GetById {

    @Test
    internal fun `Given Id - When exists - Should return Mono`() {
      // Given
      val customer = random.next<Customer>()
      every { customerRepository.findById(customer.id!!) } returns customer.toMono()

      // When-Then
      customerService.getById(customer.id!!).test()
        .expectSubscription()
        .expectNext(customer)
        .expectComplete()
        .verify()
    }

    @Test
    internal fun `Given Id - When not exists - Should return empty Mono`() {
      // Given
      every { customerRepository.findById(any<String>()) } returns Mono.empty()

      // When-Then
      customerService.getById(random.next()).test()
        .expectSubscription()
        .expectComplete()
        .verify()
    }

    @Test
    internal fun `Given Id - When error - Should return error Mono`() {
      // Given
      every { customerRepository.findById(any<String>()) } returns Mono.error { RuntimeException("boom!") }

      // When-Then
      customerService.getById(random.next()).test()
        .expectSubscription()
        .expectError()
        .verify()
    }
  }
}
