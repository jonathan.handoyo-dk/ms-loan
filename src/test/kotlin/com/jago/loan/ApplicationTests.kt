package com.jago.loan

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class ApplicationTests {

  @Test
  fun `Given Application - When starts up - Should load ApplicationContext`() {
    // no implementation here. if this test managed to run, means the context started up
  }

}
