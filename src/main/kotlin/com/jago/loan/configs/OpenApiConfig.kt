package com.jago.loan.configs

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import org.springframework.boot.info.BuildProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OpenApiConfig {

  @Bean
  fun openApi(properties: BuildProperties): OpenAPI {
    return OpenAPI()
      .info(
        Info()
          .title("${properties.group}:${properties.artifact}")
          .description(properties.name)
          .version(properties.version)
      )
  }
}
