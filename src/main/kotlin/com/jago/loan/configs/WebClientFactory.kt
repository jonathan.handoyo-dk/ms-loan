package com.jago.loan.configs

import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.http.client.HttpClient
import reactor.netty.resources.ConnectionProvider

@Component
class WebClientFactory(
  private val webClientBuilder: WebClient.Builder,
) {

  fun build(name: String, baseUrl: String, headers: Map<String, String> = emptyMap()): WebClient {
    val provider = ConnectionProvider
      .builder(name)
      .metrics(true)
      .build()

    return webClientBuilder
      .baseUrl(baseUrl)
      .defaultHeaders {
        it.set("Accept", "application/json")
        it.set("Content-Type", "application/json")
        headers.forEach { (key, value) -> it.set(key, value) }
      }
      .clientConnector(
        ReactorClientHttpConnector(
          HttpClient.create(provider)
        )
      )
      .build()
  }

}
