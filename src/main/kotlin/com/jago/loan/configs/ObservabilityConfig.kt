package com.jago.loan.configs

import io.micrometer.observation.ObservationRegistry
import io.micrometer.observation.aop.ObservedAspect
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ObservabilityConfig {

  @Bean
  fun observedAspect(observabilityRegistry: ObservationRegistry): ObservedAspect {
    return ObservedAspect(observabilityRegistry)
  }
}
