package com.jago.loan.controllers.customers.dto

data class CustomerUpdateRequest(
  val email: String?,
  val name: String?,
)
