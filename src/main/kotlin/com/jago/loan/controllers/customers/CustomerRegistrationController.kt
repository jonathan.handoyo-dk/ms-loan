package com.jago.loan.controllers.customers

import com.jago.loan.controllers.customers.dto.CustomerRegisterIdentityRequest
import com.jago.loan.domains.CustomerRegistration
import io.micrometer.observation.annotation.Observed
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@Observed
@RestController
@RequestMapping("/register")
class CustomerRegistrationController {

  @Observed
  @PostMapping
  fun startRegistration(
    @RequestBody request: CustomerRegisterIdentityRequest,
  ): Mono<CustomerRegistration> {
    return Mono.empty()
  }
}
