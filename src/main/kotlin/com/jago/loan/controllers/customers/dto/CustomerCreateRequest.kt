package com.jago.loan.controllers.customers.dto

data class CustomerCreateRequest(
  val email: String,
  val name: String,
)
