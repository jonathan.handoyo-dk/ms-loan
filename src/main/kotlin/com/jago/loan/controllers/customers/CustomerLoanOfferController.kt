package com.jago.loan.controllers.customers

import com.jago.loan.controllers.customers.dto.LoanOfferSubmitRequest
import com.jago.loan.domains.LoanApplication
import com.jago.loan.domains.LoanOffer
import com.jago.loan.services.CustomerService
import com.jago.loan.services.LoanOfferService
import io.micrometer.observation.annotation.Observed
import io.swagger.v3.oas.annotations.Operation
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Observed
@RestController
@RequestMapping("/customers/{customerId}/loans/offers")
class CustomerLoanOfferController(
  private val customerService: CustomerService,
  private val loanOfferService: LoanOfferService,
) {

  @Observed
  @Operation(description = "Gets all LoanOffer")
  @GetMapping
  fun getLoanOffers(
    @PathVariable customerId: String
  ): Flux<LoanOffer> {
    return customerService.getById(customerId)
      .flatMapMany { loanOfferService.getAllByCustomer(it.toRef()) }
  }

  @Observed
  @Operation(summary = "Get a LoanOffer")
  @GetMapping("/{offerId}")
  fun getLoanOffer(
    @PathVariable customerId: String,
    @PathVariable offerId: String,
  ): Mono<LoanOffer> {
    return Mono.empty()
  }

  @Observed
  @Operation(summary = "Submit a LoanOffer")
  @PostMapping("/{offerId}")
  fun postLoanOffer(
    @PathVariable customerId: String,
    @PathVariable offerId: String,
    @RequestBody request: LoanOfferSubmitRequest,
  ): Mono<LoanApplication> {
    return Mono.empty()
  }

  @Observed
  @Operation(summary = "Decline a LoanOffer")
  @DeleteMapping("/{offerId}")
  fun deleteLoanOffer(
    @PathVariable customerId: String,
    @PathVariable offerId: String,
  ): Mono<Void> {
    return Mono.empty()
  }
}
