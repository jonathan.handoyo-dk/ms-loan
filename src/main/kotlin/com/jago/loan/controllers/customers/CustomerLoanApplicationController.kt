package com.jago.loan.controllers.customers

import com.jago.loan.domains.LoanApplication
import io.micrometer.observation.annotation.Observed
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Observed
@RestController
@RequestMapping("/customers/{customerId}/loans/applications")
class CustomerLoanApplicationController {

  @Observed
  @GetMapping
  fun getLoanApplications(
    @PathVariable customerId: String,
  ): Flux<LoanApplication> {
    return Flux.empty()
  }

  @Observed
  @GetMapping("/{applicationId}")
  fun getLoanApplication(
    @PathVariable customerId: String,
    @PathVariable applicationId: String,
  ): Mono<LoanApplication> {
    return Mono.empty()
  }
}
