package com.jago.loan.controllers.customers.dto

import com.jago.loan.domains.LoanOfferRef

data class LoanOfferSubmitRequest(
  val id: String,
  val offer: LoanOfferRef,
)
