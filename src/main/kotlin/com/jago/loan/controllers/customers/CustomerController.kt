package com.jago.loan.controllers.customers

import com.jago.loan.controllers.customers.dto.CustomerCreateRequest
import com.jago.loan.controllers.customers.dto.CustomerUpdateRequest
import com.jago.loan.domains.Customer
import com.jago.loan.services.CustomerService
import io.micrometer.observation.annotation.Observed
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@Observed
@RestController
@RequestMapping("/customers")
class CustomerController(
  private val customerService: CustomerService
) {

  @Observed
  @GetMapping("/{customerId}")
  fun get(
    @PathVariable customerId: String
  ): Mono<Customer> {
    return Mono.empty()
  }

  @Observed
  @PostMapping
  fun post(
    @RequestBody request: CustomerCreateRequest
  ): Mono<Customer> {
    return Mono.empty()
  }

  @Observed
  @PutMapping("/{customerId}")
  fun put(
    @PathVariable customerId: String,
    @RequestBody request: CustomerUpdateRequest
  ): Mono<Customer> {
    return Mono.empty()
  }
}
