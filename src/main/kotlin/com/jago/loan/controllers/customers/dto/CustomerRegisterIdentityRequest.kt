package com.jago.loan.controllers.customers.dto

data class CustomerRegisterIdentityRequest(
  val phone: String,
  val email: String,
)
