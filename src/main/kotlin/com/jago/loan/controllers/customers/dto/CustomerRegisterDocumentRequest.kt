package com.jago.loan.controllers.customers.dto

import com.jago.loan.domains.Doc

data class CustomerRegisterDocumentRequest(
  val doc: Doc,
)
