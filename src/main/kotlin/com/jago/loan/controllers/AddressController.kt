package com.jago.loan.controllers

import com.jago.loan.domains.Region
import com.jago.loan.services.AddressService
import io.micrometer.observation.annotation.Observed
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux

@Observed
@RestController
@RequestMapping("/regions")
class AddressController(
  private val addressService: AddressService,
) {

  @Observed
  @GetMapping("/{type}")
  fun get(
    @PathVariable type: Region.Type
  ): Flux<Region> {
    return addressService.getAllByType(type)
  }

  @Observed
  @GetMapping("/{prefix}/{type}")
  fun get(
    @PathVariable prefix: String,
    @PathVariable type: Region.Type
  ): Flux<Region> {
    return addressService.getAllByPrefixAndType(prefix, type)
  }
}
