package com.jago.loan.controllers.partners.dto

import com.jago.loan.domains.LoanPartner

data class LoanPartnerCreateRequest(
  val code: String,
  val name: String,
  val emails: List<String>,
) {

  fun toLoanPartner(): LoanPartner = LoanPartner(null, code, name, emails)
}
