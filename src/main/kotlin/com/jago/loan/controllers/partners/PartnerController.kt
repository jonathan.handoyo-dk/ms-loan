package com.jago.loan.controllers.partners

import com.jago.loan.controllers.partners.dto.LoanPartnerCreateRequest
import com.jago.loan.controllers.partners.dto.LoanPartnerUpdateEmailsRequest
import com.jago.loan.domains.LoanPartner
import com.jago.loan.services.LoanOfferService
import com.jago.loan.services.LoanPartnerService
import com.jago.loan.services.LoanProductService
import io.micrometer.observation.annotation.Observed
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Observed
@RestController
@RequestMapping("/partners")
class PartnerController(
  private val loanPartnerService: LoanPartnerService,
  private val loanProductService: LoanProductService,
  private val loanOfferService: LoanOfferService,
) {

  @Observed
  @GetMapping
  fun get(): Flux<LoanPartner> {
    return loanPartnerService.getAll()
  }

  @Observed
  @GetMapping("/{partnerId}")
  fun get(
    @PathVariable partnerId: String
  ): Mono<LoanPartner> {
    return loanPartnerService.getById(partnerId)
  }

  @Observed
  @PostMapping
  fun post(
    @RequestBody request: LoanPartnerCreateRequest
  ): Mono<LoanPartner> {
    return loanPartnerService.create(request)
  }

  @Observed
  @PutMapping("/{partnerId}/email")
  fun putEmails(
    @PathVariable partnerId: String,
    @RequestBody request: LoanPartnerUpdateEmailsRequest
  ): Mono<LoanPartner> {
    return loanPartnerService.updateById(partnerId, request)
  }
}
