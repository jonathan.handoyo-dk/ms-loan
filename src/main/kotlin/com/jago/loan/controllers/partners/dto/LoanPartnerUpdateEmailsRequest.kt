package com.jago.loan.controllers.partners.dto

data class LoanPartnerUpdateEmailsRequest(
  val emails: List<String>,
)
