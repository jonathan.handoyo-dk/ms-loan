package com.jago.loan.controllers.partners

import com.jago.loan.controllers.partners.dto.LoanOfferCreateRequest
import com.jago.loan.domains.LoanOffer
import com.jago.loan.services.LoanOfferService
import com.jago.loan.services.LoanPartnerService
import com.jago.loan.services.LoanProductService
import io.micrometer.observation.annotation.Observed
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Observed
@RestController
@RequestMapping("/partners/{partnerId}/loans/products/{productId}/offers")
class PartnerLoanOfferController(
  private val loanPartnerService: LoanPartnerService,
  private val loanProductService: LoanProductService,
  private val loanOfferService: LoanOfferService,
) {

  @Observed
  @GetMapping
  fun getLoanOffers(
    @PathVariable partnerId: String,
    @PathVariable productId: String
  ): Flux<LoanOffer> {
    return loanPartnerService.getById(partnerId)
      .flatMap { loanProductService.getById(it.toRef(), productId) }
      .flatMapMany { loanOfferService.getAllByLoanProduct(it.toRef()) }
  }

  @Observed
  @PostMapping
  fun postLoanOffer(
    @PathVariable partnerId: String,
    @PathVariable productId: String,
    @RequestBody request: LoanOfferCreateRequest
  ): Mono<LoanOffer> {
    return Mono.empty()
  }

  @Observed
  @DeleteMapping("/{offerId}")
  fun deleteLoanOffer(
    @PathVariable partnerId: String,
    @PathVariable productId: String,
    @PathVariable offerId: String
  ): Mono<LoanOffer> {
    return Mono.empty()
  }
}
