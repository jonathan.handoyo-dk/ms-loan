package com.jago.loan.controllers.partners.dto

import com.jago.loan.domains.CustomerRef
import com.jago.loan.domains.LoanOffer
import com.jago.loan.domains.LoanProductRef
import java.time.LocalDate

data class LoanOfferCreateRequest(
  val id: String,
  val customer: CustomerRef,
  val product: LoanProductRef,
  val customisation: LoanOffer.Customisation,
  val expiredDate: LocalDate,
) {

  fun toLoanOffer(): LoanOffer {
    return LoanOffer(
      null,
      customer,
      product,
      LoanOffer.Status.ACTIVE,
      customisation,
      expiredDate
    )
  }
}
