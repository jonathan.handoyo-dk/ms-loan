package com.jago.loan.controllers.partners.dto

data class LoanProductCreateRequest(
  val code: String
)
