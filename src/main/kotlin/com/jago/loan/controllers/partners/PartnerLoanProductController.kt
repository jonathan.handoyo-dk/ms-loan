package com.jago.loan.controllers.partners

import com.jago.loan.controllers.partners.dto.LoanProductCreateRequest
import com.jago.loan.controllers.partners.dto.LoanProductUpdateRequest
import com.jago.loan.domains.LoanProduct
import com.jago.loan.services.LoanOfferService
import com.jago.loan.services.LoanPartnerService
import com.jago.loan.services.LoanProductService
import io.micrometer.observation.annotation.Observed
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Observed
@RestController
@RequestMapping("/partners/{partnerId}/loans/products")
class PartnerLoanProductController(
  private val loanPartnerService: LoanPartnerService,
  private val loanProductService: LoanProductService,
  private val loanOfferService: LoanOfferService,
) {

  @Observed
  @GetMapping
  fun getLoanProducts(
    @PathVariable partnerId: String
  ): Flux<LoanProduct> {
    return Flux.empty()
  }

  @Observed
  @GetMapping("/{productId}")
  fun getLoanProduct(
    @PathVariable partnerId: String,
    @PathVariable productId: String
  ): Mono<LoanProduct> {
    return Mono.empty()
  }

  @Observed
  @PostMapping
  fun postLoanProduct(
    @PathVariable partnerId: String,
    @RequestBody request: LoanProductCreateRequest
  ): Mono<LoanProduct> {
    return Mono.empty()
  }

  @Observed
  @PutMapping("/{productId}")
  fun putLoanProduct(
    @PathVariable partnerId: String,
    @PathVariable productId: String,
    @RequestBody request: LoanProductUpdateRequest
  ): Mono<LoanProduct> {
    return Mono.empty()
  }
}
