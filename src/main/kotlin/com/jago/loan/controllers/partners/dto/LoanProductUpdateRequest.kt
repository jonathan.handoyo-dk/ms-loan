package com.jago.loan.controllers.partners.dto

data class LoanProductUpdateRequest(
  val code: String
)
