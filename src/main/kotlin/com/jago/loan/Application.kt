package com.jago.loan

import io.micrometer.observation.annotation.Observed
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.mongodb.config.EnableReactiveMongoAuditing
import reactor.core.publisher.Hooks

typealias UpdatedDate = LastModifiedDate

@Observed
@ConfigurationPropertiesScan
@EnableReactiveMongoAuditing
@SpringBootApplication
class Application

@Observed
fun main(args: Array<String>) {
  Hooks.enableAutomaticContextPropagation()
  runApplication<Application>(*args)
}
