package com.jago.loan.services

import com.jago.loan.controllers.partners.dto.LoanProductCreateRequest
import com.jago.loan.controllers.partners.dto.LoanProductUpdateRequest
import com.jago.loan.domains.LoanPartnerRef
import com.jago.loan.domains.LoanProduct
import com.jago.loan.domains.LoanProductRepository
import io.micrometer.observation.annotation.Observed
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Observed
@Service
class LoanProductService(
  private val loanProductRepository: LoanProductRepository,
) {

  @Observed
  fun getAllByLoanPartner(partner: LoanPartnerRef): Flux<LoanProduct> {
    return loanProductRepository.findAllByPartner(partner)
  }

  @Observed
  fun getById(partner: LoanPartnerRef, id: String): Mono<LoanProduct> {
    return loanProductRepository.findByPartnerAndId(partner, id)
  }

  @Observed
  fun create(request: LoanProductCreateRequest): Mono<LoanProduct> {
    return Mono.empty()
  }

  @Observed
  fun updateById(id: String, request: LoanProductUpdateRequest): Mono<LoanProduct> {
    return Mono.empty()
  }
}
