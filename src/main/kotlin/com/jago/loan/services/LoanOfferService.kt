package com.jago.loan.services

import com.jago.loan.domains.CustomerRef
import com.jago.loan.domains.LoanOffer
import com.jago.loan.domains.LoanOfferRepository
import com.jago.loan.domains.LoanProductRef
import io.micrometer.observation.annotation.Observed
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux

@Observed
@Service
class LoanOfferService(
  private val loanOfferRepository: LoanOfferRepository,
) {

  @Observed
  fun getAllByCustomer(customer: CustomerRef): Flux<LoanOffer> {
    return loanOfferRepository.findAllByCustomer(customer)
  }

  @Observed
  fun getAllByLoanProduct(product: LoanProductRef): Flux<LoanOffer> {
    return loanOfferRepository.findAllByProduct(product)
  }

}
