package com.jago.loan.services

import com.jago.loan.controllers.customers.dto.CustomerRegisterDocumentRequest
import com.jago.loan.controllers.customers.dto.CustomerRegisterIdentityRequest
import com.jago.loan.domains.CustomerRegistration
import com.jago.loan.domains.CustomerRegistrationRepository
import io.micrometer.observation.annotation.Observed
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@Observed
@Service
class RegistrationService(
  private val customerRegistrationRepository: CustomerRegistrationRepository,
  private val customerService: CustomerService,
) {

  @Observed
  fun registerIdentity(request: CustomerRegisterIdentityRequest): Mono<CustomerRegistration> {
    val identity = CustomerRegistration.Identity(request.email, request.phone)
    return customerRegistrationRepository.existsByIdentity(identity)
      .flatMap { exists ->
        when (!exists) {
          true -> CustomerRegistration(identity = identity).toMono()
          else -> Mono.error { RegistrationExistsException(request.email, request.phone) }
        }
      }
      .flatMap { customerRegistrationRepository.save(it) }
  }

  @Observed
  fun registerDocument(registrationId: String, request: CustomerRegisterDocumentRequest): Mono<CustomerRegistration> {
    return customerRegistrationRepository.findById(registrationId)
      .map { it.copy() }
  }
}


class RegistrationExistsException(email: String, phone: String) :
  Exception("Registration already exists. Registration(email=$email, phone=$phone)")
