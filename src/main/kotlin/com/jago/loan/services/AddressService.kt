package com.jago.loan.services

import com.jago.loan.domains.Region
import com.jago.loan.domains.RegionRepository
import io.micrometer.observation.annotation.Observed
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux

@Observed
@Service
class AddressService(
  private val regionRepository: RegionRepository,
) {

  @Observed
  fun getAllByType(type: Region.Type): Flux<Region> {
    return regionRepository.findAllByType(type)
  }

  @Observed
  fun getAllByPrefixAndType(prefix: String, type: Region.Type): Flux<Region> {
    return regionRepository.findAllByTypeAndCodeStartingWith(type, prefix)
  }
}
