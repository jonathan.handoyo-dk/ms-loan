package com.jago.loan.services

import com.jago.loan.domains.CustomerRef
import com.jago.loan.domains.LoanApplication
import com.jago.loan.domains.LoanApplicationRepository
import io.micrometer.observation.annotation.Observed
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux

@Observed
@Service
class LoanApplicationService(
  private val loanApplicationRepository: LoanApplicationRepository,
) {

  @Observed
  fun getAllByCustomer(customer: CustomerRef): Flux<LoanApplication> {
    return loanApplicationRepository.findAllByCustomer(customer)
  }

  @Observed
  fun getAllByCustomerAndStatus(customer: CustomerRef, status: LoanApplication.Status): Flux<LoanApplication> {
    return loanApplicationRepository.findAllByCustomerAndStatus(customer, status)
  }
}
