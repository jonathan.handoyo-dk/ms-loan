package com.jago.loan.services

import com.jago.loan.domains.Customer
import com.jago.loan.domains.CustomerRepository
import com.jago.loan.domains.Doc
import io.micrometer.observation.annotation.Observed
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Observed
@Service
class CustomerService(
  private val customerRepository: CustomerRepository,
) {

  @Observed
  fun getById(id: String): Mono<Customer> {
    return customerRepository.findById(id)
  }

  @Observed
  fun getByDoc(doc: Doc): Mono<Customer> {
    return customerRepository.findByDoc(doc)
  }
}
