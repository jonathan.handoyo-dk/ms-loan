package com.jago.loan.services

import com.jago.loan.controllers.partners.dto.LoanPartnerCreateRequest
import com.jago.loan.controllers.partners.dto.LoanPartnerUpdateEmailsRequest
import com.jago.loan.domains.LoanPartner
import com.jago.loan.domains.LoanPartnerRepository
import io.micrometer.observation.annotation.Observed
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Observed
@Service
class LoanPartnerService(
  private val loanPartnerRepository: LoanPartnerRepository
) {

  @Observed
  fun getAll(): Flux<LoanPartner> {
    return loanPartnerRepository.findAll()
  }

  @Observed
  fun getById(id: String): Mono<LoanPartner> {
    return loanPartnerRepository.findById(id)
  }

  @Observed
  fun getByCode(code: String): Mono<LoanPartner> {
    return loanPartnerRepository.findByCode(code)
  }

  @Observed
  fun create(request: LoanPartnerCreateRequest): Mono<LoanPartner> {
    return loanPartnerRepository.insert(request.toLoanPartner())
  }

  @Observed
  fun updateById(id: String, request: LoanPartnerUpdateEmailsRequest): Mono<LoanPartner> {
    return loanPartnerRepository.findById(id)
      .map { it.copy(emails = it.emails + request.emails) }
      .flatMap { loanPartnerRepository.save(it) }
  }
}
