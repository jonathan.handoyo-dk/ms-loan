package com.jago.loan.domains

import com.jago.loan.UpdatedDate
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Version
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Instant

@Document
data class Region(

  @Id
  val code: String,
  val type: Type,
  val name: Name,

  @CreatedDate val createdDate: Instant? = null,
  @UpdatedDate val updatedDate: Instant? = null,
  @Version val version: Int? = null,
) {

  enum class Type {
    COUNTRY,
    PROVINCE,
    CITY,
    DISTRICT,
    SUBDISTRICT,
  }

  data class Name(
    val id: String,
    val en: String,
  )

}

@Repository
interface RegionRepository : ReactiveMongoRepository<Region, String> {

  fun findByTypeAndCode(type: Region.Type, code: String): Mono<Region>
  fun findAllByType(type: Region.Type): Flux<Region>
  fun findAllByTypeAndCodeStartingWith(type: Region.Type, prefix: String): Flux<Region>
}
