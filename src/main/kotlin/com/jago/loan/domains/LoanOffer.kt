package com.jago.loan.domains

import com.jago.loan.UpdatedDate
import nonapi.io.github.classgraph.json.Id
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Version
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import java.time.Duration
import java.time.Instant
import java.time.LocalDate

@Document
data class LoanOffer(

  @Id
  val id: String?,
  val customer: CustomerRef,
  val product: LoanProductRef,

  val status: Status,
  val customisation: Customisation,
  val expiredDate: LocalDate,

  @CreatedDate val createdDate: Instant? = null,
  @UpdatedDate val updatedDate: Instant? = null,
  @Version val version: Int? = null,
) {

  fun toRef(): LoanOfferRef = LoanOfferRef(id!!)

  enum class Status {
    ACTIVE,
    INACTIVE,
    REDEEMED,
    WITHDRAWN,
  }

  data class Customisation(
    val maxAmount: Double,
    val maxTenure: Duration,
    val interestRate: Double,
  )
}

data class LoanOfferRef(
  val id: String,
)

@Repository
interface LoanOfferRepository : ReactiveMongoRepository<LoanOffer, String> {
  fun findAllByCustomer(customer: CustomerRef): Flux<LoanOffer>
  fun findAllByProduct(product: LoanProductRef): Flux<LoanOffer>
}
