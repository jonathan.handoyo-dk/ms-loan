package com.jago.loan.domains

import com.jago.loan.UpdatedDate
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Version
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import java.time.Instant

@Document
data class LoanPartner(

  @Id
  val id: String?,

  val code: String,
  val name: String,

  val emails: List<String>,

  @CreatedDate val createdDate: Instant? = null,
  @UpdatedDate val updatedDate: Instant? = null,
  @Version val version: Int? = null,
) {

  fun toRef(): LoanPartnerRef = LoanPartnerRef(id!!, code, name)
}

data class LoanPartnerRef(
  val id: String,
  val code: String,
  val name: String,
)

@Repository
interface LoanPartnerRepository : ReactiveMongoRepository<LoanPartner, String> {

  fun findByCode(code: String): Mono<LoanPartner>
}
