package com.jago.loan.domains

import com.jago.loan.UpdatedDate
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Version
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import java.time.Instant

@Document
data class LoanApplication(

  @Id
  val id: String?,

  val customer: CustomerRef,
  val offer: LoanOfferRef,

  val loanAmount: Double,
  val loanTenor: Int,
  val loanRepaymentDate: Int,

  val status: Status,

  @CreatedDate val createdDate: Instant? = null,
  @UpdatedDate val updatedDate: Instant? = null,
  @Version val version: Int? = null,
) {

  enum class Status {
    SUBMITTED,
    PENDING,
    APPROVED,
    REJECTED,
    ACTIVE,
  }
}

@Repository
interface LoanApplicationRepository : ReactiveMongoRepository<LoanApplication, String> {

  fun findAllByCustomer(customer: CustomerRef): Flux<LoanApplication>
  fun findAllByCustomerAndStatus(customer: CustomerRef, status: LoanApplication.Status): Flux<LoanApplication>
}
