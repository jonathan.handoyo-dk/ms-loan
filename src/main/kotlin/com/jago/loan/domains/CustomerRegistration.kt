package com.jago.loan.domains

import com.jago.loan.UpdatedDate
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Version
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import java.time.Instant

@Document
data class CustomerRegistration(

  @Id
  val id: String? = null,

  val identity: Identity? = null,
  val documents: List<Doc>? = null,

  @CreatedDate val createdDate: Instant? = null,
  @UpdatedDate val updatedDate: Instant? = null,
  @Version val version: Int? = null,
) {

  data class Identity(
    val email: String,
    val phone: String,
  )
}

@Repository
interface CustomerRegistrationRepository : ReactiveMongoRepository<CustomerRegistration, String> {

  fun existsByIdentity(identity: CustomerRegistration.Identity): Mono<Boolean>
}
