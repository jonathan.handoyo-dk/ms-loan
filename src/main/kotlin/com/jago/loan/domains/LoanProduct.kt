package com.jago.loan.domains

import com.jago.loan.UpdatedDate
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Version
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Instant

@Document
data class LoanProduct(

  @Id
  val id: String,
  val partner: LoanPartnerRef,

  @CreatedDate val createdDate: Instant? = null,
  @UpdatedDate val updatedDate: Instant? = null,
  @Version val version: Int? = null,
) {

  fun toRef(): LoanProductRef = LoanProductRef(id)
}

data class LoanProductRef(
  val id: String
)

@Repository
interface LoanProductRepository : ReactiveMongoRepository<LoanProduct, String> {
  fun findAllByPartner(partner: LoanPartnerRef): Flux<LoanProduct>
  fun findByPartnerAndId(partner: LoanPartnerRef, id: String): Mono<LoanProduct>
}
