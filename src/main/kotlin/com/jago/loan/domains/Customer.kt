package com.jago.loan.domains

import com.jago.loan.UpdatedDate
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Version
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import java.time.Instant

@Document
data class Customer(

  @Id
  val id: String?,

  val cif: String,
  val email: String,
  val phone: String,
  val name: String,

  val doc: Doc,
  val address: Address,

  @CreatedDate val createdDate: Instant? = null,
  @UpdatedDate val updatedDate: Instant? = null,
  @Version val version: Int? = null,
) {

  fun toRef(): CustomerRef = CustomerRef(id!!, name)
}

data class Address(
  val province: Region,
  val city: Region,
  val district: Region,
  val subdistrict: Region,
)

data class Doc(
  val type: Type,
  val number: String,
  val url: String,
) {

  enum class Type {
    PASSPORT,
    KTP,
    KITAS,
    KITAP,
  }
}

data class CustomerRef(
  val id: String,
  val name: String,
)

@Repository
interface CustomerRepository : ReactiveMongoRepository<Customer, String> {

  fun findByDoc(doc: Doc): Mono<Customer>
  fun findByCif(cif: String): Mono<Customer>
  fun findByEmailAndPhone(email: String, phone: String): Mono<Customer>
}
