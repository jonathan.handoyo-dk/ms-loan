import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  id("idea")
  id("org.springframework.boot") version "3.1.2"
  id("io.spring.dependency-management") version "1.1.2"
  kotlin("jvm") version "1.8.22"
  kotlin("plugin.spring") version "1.8.22"
}

group = "com.jago"
version = "0.0.1-SNAPSHOT"

java {
  sourceCompatibility = JavaVersion.VERSION_17
}

configurations {
  compileOnly {
    extendsFrom(configurations.annotationProcessor.get())
  }
}

repositories {
  mavenCentral()
}

object Versions {
  const val easyRandom = "5.0.0"
  const val kotlinLogging = "3.0.5"
  const val mockk = "1.13.5"
  const val nettyResolver = "4.1.94.Final"
  const val openApi = "2.0.4"
}

dependencies {
  // Kotlin
  implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")
  implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
  implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
  implementation("org.jetbrains.kotlin:kotlin-reflect")
  implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")

  // Spring
  implementation("org.springframework.boot:spring-boot-starter-actuator")
  implementation("org.springframework.boot:spring-boot-starter-aop")
  implementation("org.springframework.boot:spring-boot-starter-data-mongodb-reactive")
  implementation("org.springframework.boot:spring-boot-starter-webflux")
  annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")

  // Misc
  implementation("ch.qos.logback:logback-classic")
  implementation("org.springdoc:springdoc-openapi-starter-webflux-ui:${Versions.openApi}")
  implementation("io.kotest:kotest-assertions-core:5.7.1")
  implementation("io.micrometer:micrometer-registry-prometheus")
  implementation("io.micrometer:micrometer-tracing-bridge-brave")
  implementation("io.github.microutils:kotlin-logging-jvm:${Versions.kotlinLogging}")
  implementation("net.logstash.logback:logstash-logback-encoder:7.3")

  // Test
  testImplementation("com.squareup.okhttp3:mockwebserver")
  testImplementation("org.jeasy:easy-random-core:${Versions.easyRandom}")
  testImplementation("org.springframework.boot:spring-boot-starter-test")
  testImplementation("io.mockk:mockk:${Versions.mockk}")
  testImplementation("io.projectreactor:reactor-test")

  // Runtime
  runtimeOnly("io.netty:netty-resolver-dns-native-macos:${Versions.nettyResolver}:osx-aarch_64")
  runtimeOnly("io.netty:netty-resolver-dns-native-macos:${Versions.nettyResolver}:osx-x86_64")

}

idea {
  module {
    isDownloadJavadoc = true
    isDownloadSources = true
  }
}

tasks {
  withType<KotlinCompile> {
    kotlinOptions {
      freeCompilerArgs += "-Xjsr305=strict"
      jvmTarget = "17"
    }
  }

  withType<Test> {
    useJUnitPlatform()
  }

  springBoot {
    buildInfo()
  }


}
